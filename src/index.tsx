import 'modern-normalize/modern-normalize.css'
import './styles/index.scss'
import './styles/App.scss'

import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import WebFont from 'webfontloader'

import { App } from './Components/App'
import * as serviceWorker from './serviceWorker'

WebFont.load({
   google: {
      families: ['Roboto']
   }
})

const Root = () => {
   return (
      <Fragment>
         <Helmet>
            <title>Javascript Calculator</title>
            <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>
         </Helmet>
         <App />
      </Fragment>
   )
}

ReactDOM.render(<Root />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
