import { none, some } from 'fp-ts/lib/Option'

import { ALL_CLEAR, calculatorButtons, DECIMAL, MATH_OPERATORS, ZERO } from './constants'
import { removeTrailingDecimal } from './formatting'
import {
   formulaHasBeenCalculated,
   isNegative,
   isOperator,
   removeFinalOperator
} from './utils'

export type Params = {
   buttonValue: string
   inputText: string
   formulaText: string
}

/**
 *
 *
 * reset the input and formula from the display
 */
const handleAllClear = (buttonValue: string) => {
   if (buttonValue === ALL_CLEAR) {
      return some({
         inputText: '',
         formulaText: ''
      })
   }

   return none
}

/**
 *
 *
 * toggle the input between negative and positive
 */
const toggleInputValueSign = ({ buttonValue, inputText, formulaText }: Params) => {
   if (
      buttonValue === MATH_OPERATORS.negation ||
      (buttonValue === MATH_OPERATORS.minus && inputText === '')
   ) {
      const nextInputText = isNegative(inputText.charAt(0))
         ? inputText.slice(1)
         : `${MATH_OPERATORS.minus}${inputText}`

      return some({
         inputText: nextInputText,
         formulaText
      })
   }

   return none
}

/**
 *
 *
 * add the operator to the end of the formula text
 */
const appendOperator = ({ buttonValue, inputText, formulaText }: Params) => {
   if (!isOperator(buttonValue) || formulaHasBeenCalculated(formulaText)) return none

   // we need to use the asterisk so that mathjs can calculate multiplication
   const operator = buttonValue === MATH_OPERATORS.multiply ? '*' : buttonValue

   let nextFormulaText = `${formulaText} ${removeTrailingDecimal(inputText)} ${operator}`

   // if the input text is just the minus sign, then it's an empty negation
   // and we treat it as an actual empty value
   if (inputText === '' || inputText === MATH_OPERATORS.minus) {
      //
      // if the input is empty, but we have a current formula, we assume the
      // user wants to change the last entered operator
      nextFormulaText =
         formulaText === '' ? '' : `${removeFinalOperator(formulaText)} ${operator}`
   }

   return some({
      inputText: '',
      formulaText: nextFormulaText
   })
}

export {
   appendOperator,
   calculatorButtons,
   handleAllClear,
   ZERO,
   MATH_OPERATORS,
   DECIMAL,
   toggleInputValueSign
}
