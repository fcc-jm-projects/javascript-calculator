import { none, some } from 'fp-ts/lib/Option'
import { evaluate } from 'mathjs'

import { EQUALS } from './constants'
import { MATH_OPERATORS, Params } from './operations'
import { formulaHasBeenCalculated, isOperator, removeFinalOperator } from './utils'

/**
 *
 *
 * replace the unicode division symbol with '/'
 */
const replaceDivisionSymbol = (text: string) =>
   text.replace(new RegExp(`${MATH_OPERATORS.divide}`, 'g'), '/')

/**
 *
 *
 * calculate the expression
 */
const calculateFormula = ({ buttonValue, inputText, formulaText }: Params) => {
   if (buttonValue !== EQUALS) return none

   // sometimes the user may try to evaluate the formula after having pressed
   // an operation key. So we remove it for them :)
   const withoutTrailingOperator =
      inputText === '' ? removeFinalOperator(formulaText) : formulaText

   const sanitizedFormula = `${withoutTrailingOperator} ${inputText}`

   const result = evaluate(replaceDivisionSymbol(sanitizedFormula))

   return some({
      inputText: result,
      formulaText: `${sanitizedFormula} ${EQUALS} ${result}`
   })
}

/**
 *
 *
 * continue the calculation using the result of the previous calculation
 */
const continueCalculation = ({ buttonValue, formulaText }: Params) => {
   if (formulaHasBeenCalculated(formulaText) && isOperator(buttonValue)) {
      const matches = /(?:=)(.+)/.exec(formulaText)
      const result = matches ? matches[1] : ''

      return some({
         inputText: '',
         formulaText: `${result} ${buttonValue}`
      })
   }

   return none
}

/**
 *
 *
 * start a brand new calculation
 */
const startNewCalculation = ({ buttonValue, formulaText }: Params) => {
   if (formulaHasBeenCalculated(formulaText) && !isOperator(buttonValue)) {
      return some({
         inputText: buttonValue,
         formulaText: ''
      })
   }

   return none
}

export { calculateFormula, continueCalculation, startNewCalculation }
