import { none, some } from 'fp-ts/lib/Option'

import { DECIMAL, MATH_OPERATORS, Params, ZERO } from './operations'

/**
 *
 *
 * remove all leading zeros from the input text
 */
const removeLeadingZeros = ({ buttonValue, inputText, formulaText }: Params) => {
   const leadingNumber =
      inputText.charAt(0) === MATH_OPERATORS.minus
         ? inputText.charAt(1)
         : inputText.charAt(0)

   if (buttonValue !== ZERO || leadingNumber !== '') return none

   return some({
      inputText,
      formulaText
   })
}

/**
 *
 *
 * remove all subsequent decimals
 */
const removeMultipleDecimals = ({ buttonValue, inputText, formulaText }: Params) => {
   const inputHasDecimal = inputText.includes(DECIMAL)

   if (buttonValue !== DECIMAL || !inputHasDecimal) return none

   return some({
      inputText,
      formulaText
   })
}

/**
 *
 *
 * add a leading zero if a decimal is the first character
 */
const addLeadingZeroToDecimal = ({ buttonValue, inputText, formulaText }: Params) => {
   const leadingValue =
      inputText.charAt(0) === MATH_OPERATORS.minus
         ? inputText.charAt(1)
         : inputText.charAt(0)

   if (buttonValue !== DECIMAL || leadingValue !== '') return none

   return some({
      inputText: `0${DECIMAL}`,
      formulaText
   })
}

/**
 *
 *
 * remove the decimal if it's the last char of the text
 */
const removeTrailingDecimal = (formulaText: string) => {
   return formulaText.charAt(formulaText.length - 1) === DECIMAL
      ? formulaText.substring(0, formulaText.length - 1)
      : formulaText
}

export {
   addLeadingZeroToDecimal,
   removeLeadingZeros,
   removeMultipleDecimals,
   removeTrailingDecimal
}
