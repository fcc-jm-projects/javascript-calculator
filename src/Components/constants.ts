type OPERATORS = {
   divide: string
   multiply: string
   minus: string
   plus: string
   negation: string
}

const MATH_OPERATORS: OPERATORS = {
   divide: '\u00F7',
   multiply: 'x',
   minus: '-',
   plus: '+',
   negation: '\u00B1'
}

const EQUALS = '='
const ALL_CLEAR = 'ac'
const DECIMAL = '.'
const ZERO = '0'

const calculatorButtons = [
   // [text , classname, id]
   [ALL_CLEAR, 'button--clear button--operator', 'clear'],
   [MATH_OPERATORS.negation, 'button--operator', 'negation'],
   [MATH_OPERATORS.divide, 'button--divide button--operator', 'divide'],
   [MATH_OPERATORS.multiply, 'button--mult button--operator', 'multiply'],
   ['7', 'button--number', 'seven'],
   ['8', 'button--number', 'eight'],
   ['9', 'button--number', 'nine'],
   [MATH_OPERATORS.minus, 'button--minus button--operator', 'subtract'],
   ['4', 'button--number', 'four'],
   ['5', 'button--number', 'five'],
   ['6', 'button--number', 'six'],
   [MATH_OPERATORS.plus, 'button--plus button--operator', 'add'],
   ['1', 'button--number', 'one'],
   ['2', 'button--number', 'two'],
   ['3', 'button--number', 'three'],
   [EQUALS, 'button--equals', 'equals'],
   [DECIMAL, 'button--decimal', 'decimal'],
   [ZERO, 'button--number button--zero', 'zero']
]

export { calculatorButtons, MATH_OPERATORS, EQUALS, ALL_CLEAR, DECIMAL, ZERO }
