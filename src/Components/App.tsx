import React, { useState } from 'react'

import {
   calculateFormula,
   continueCalculation,
   startNewCalculation
} from './calculations'
import {
   addLeadingZeroToDecimal,
   removeLeadingZeros,
   removeMultipleDecimals
} from './formatting'
import {
   appendOperator,
   calculatorButtons,
   handleAllClear,
   toggleInputValueSign
} from './operations'

const App: React.FC = () => {
   const [inputText, setinputText] = useState('')
   const [formulaText, setFormulaText] = useState('')

   const handleOnClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.preventDefault()
      const buttonValue = (e.target as HTMLButtonElement).innerText

      // FCC passes in numbers as input, so we must convert to strings to pass
      // since our logic uses mathjs to evaluate strings
      const stringifiedInput = String(inputText)

      const params = {
         buttonValue,
         inputText: stringifiedInput,
         formulaText
      }

      const nextText = handleAllClear(buttonValue)
         .alt(removeLeadingZeros(params))
         .alt(removeMultipleDecimals(params))
         .alt(addLeadingZeroToDecimal(params))
         .alt(toggleInputValueSign(params))
         .alt(appendOperator(params))
         .alt(calculateFormula(params))
         .alt(continueCalculation(params))
         .alt(startNewCalculation(params))
         .getOrElse({
            inputText: `${inputText}${buttonValue}`,
            formulaText: `${formulaText}`
         })

      setinputText(nextText.inputText)
      setFormulaText(nextText.formulaText)
   }

   return (
      <div className="app-container">
         <main className="calculator-container">
            <div className="display">
               <div className="display__input" id="display">
                  {inputText || '0'}
               </div>
               <div className="display__formula">{formulaText}</div>
            </div>
            <div className="inputs">
               {calculatorButtons.map((b) => {
                  const [buttonValue, className, id] = b
                  return (
                     <button
                        className={`button ${className}`}
                        key={id}
                        id={id}
                        onClick={handleOnClick}
                     >
                        {buttonValue}
                     </button>
                  )
               })}
            </div>
         </main>
      </div>
   )
}

export { App }
