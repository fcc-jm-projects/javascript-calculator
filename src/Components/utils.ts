import { EQUALS, MATH_OPERATORS } from './constants'

/**
 *
 *
 * check if the formula has already been calculated
 */
const formulaHasBeenCalculated = (formulaText: string) => formulaText.includes(EQUALS)

/**
 *
 *
 * check if the character is the minus (-) sign
 */
const isNegative = (char: string) => char === MATH_OPERATORS.minus

/**
 *
 *
 * check if the value is a math operator
 */
const isOperator = (text: string) => {
   return (
      text === MATH_OPERATORS.divide ||
      text === MATH_OPERATORS.plus ||
      text === MATH_OPERATORS.minus ||
      text === MATH_OPERATORS.multiply ||
      // we use the asterisk in the formula text to allow evaluation
      text === '*'
   )
}

/**
 *
 *
 * remove the final operator from the string
 */
const removeFinalOperator = (text: string) => {
   const lastChar = text.charAt(text.length - 1)

   return isOperator(lastChar) ? text.slice(0, text.length - 1) : text
}

export { formulaHasBeenCalculated, isNegative, isOperator, removeFinalOperator }
